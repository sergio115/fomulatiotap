package com.ponysoft.formulariotap;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    String carrera, genero, preferencias = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText nameEdTx = findViewById(R.id.nameEdTx);
        final EditText lastNameEdTx = findViewById(R.id.lastNameEdTx);
        final EditText motherLastNameEdTx = findViewById(R.id.motherLastNameEdTx);
        final CheckBox sportsCkBx = findViewById(R.id.sportsCkBx);
        final CheckBox musicCkBx = findViewById(R.id.musicCkBx);
        final CheckBox travelsCkBx = findViewById(R.id.travelsCkBx);
        final RadioGroup sexRdGp = findViewById(R.id.sexRdGp);
        final TextView setNameEdTx = findViewById(R.id.setNameTxVw);
        final EditText careerEdTx = findViewById(R.id.careerEdTx);
        final EditText likesEdTx = findViewById(R.id.likesEdTx);
        final EditText genderEdTx = findViewById(R.id.genderEdtX);
        final Button acceptBtn = findViewById(R.id.acceptBtn);
        Spinner careerSpinner = findViewById(R.id.careerSpinner);
        final ArrayList<String> careerAyList = new ArrayList<>();
        careerAyList.add("Sistemas");
        careerAyList.add("TIC´s");
        careerAyList.add("Informatica");
        careerAyList.add("Civil");
        careerAyList.add("Administración");
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_list_item_1, careerAyList);
        careerSpinner.setAdapter(adapter);
        careerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView tv = (TextView) view;
                carrera = tv.getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sexRdGp.getCheckedRadioButtonId() == R.id.Male) {
                    genero = "Masculino";
                }
                if (sexRdGp.getCheckedRadioButtonId() == R.id.Female) {
                    genero = "Femenino";
                }
                genderEdTx.setText(genero);
                careerEdTx.setText(carrera);
                if(sportsCkBx.isChecked()) {
                    preferencias = preferencias + " Deportes ";
                }
                if(travelsCkBx.isChecked()) {
                    preferencias = preferencias + " Viajes ";
                }
                if(musicCkBx.isChecked()) {
                    preferencias = preferencias + " Música ";
                }
                likesEdTx.setText(preferencias);
                setNameEdTx.setText(
                        nameEdTx.getText() + " " +
                        lastNameEdTx.getText() + " " +
                        motherLastNameEdTx.getText());
            }
        });
    }
}
